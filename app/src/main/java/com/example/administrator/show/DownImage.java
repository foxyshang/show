package com.example.administrator.show;

import java.net.URL;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
/**
 * ������ȡ����ͼƬ
 * @author Administrator
 *
 */
public class DownImage {
	public String image_path;

	public DownImage(String image_path) {
		this.image_path = image_path;
	}
//���Ǻ�����
	//����
	public void loadImage(final ImageCallBack callBack) 
	{

		final Handler handler = new Handler() {
			@Override
			public void handleMessage(Message msg) {
				super.handleMessage(msg);
				Drawable drawable = (Drawable) msg.obj;
				// �ӷ�������ȡ��ͼƬ��handler��  handler��ͼƬ���� �ӿڵ�ʵ�ַ�����
				callBack.getDrawable(drawable);
			}
		};

		new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					Drawable drawable = Drawable.createFromStream(new URL(
							image_path).openStream(), "");
					Log.v("show","imurl::"+image_path);
					Message message = Message.obtain();
					message.obj = drawable;
					handler.sendMessage(message);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}).start();
	}

	public interface ImageCallBack {
		//ֻ�� �����壬û��ʵ�֡�
		public void getDrawable(Drawable drawable);
	}
}
