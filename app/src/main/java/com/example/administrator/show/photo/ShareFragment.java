package com.example.administrator.show.photo;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.administrator.show.HomeChildFragment;
import com.example.administrator.show.R;
import com.example.administrator.show.data.ImageList;
import com.example.administrator.show.utils.ImageResizer;
import com.umeng.socialize.ShareAction;
import com.umeng.socialize.UMShareListener;
import com.umeng.socialize.bean.SHARE_MEDIA;

//import org.greenrobot.eventbus.EventBus;
//import org.greenrobot.eventbus.Subscribe;
///import org.greenrobot.eventbus.ThreadMode;

import java.io.File;

///import com.umeng.socialize.ShareAction;
///import com.umeng.socialize.UMShareAPI;
///import com.umeng.socialize.UMShareListener;
//import com.umeng.socialize.bean.SHARE_MEDIA;

/**
 * @author 这个页面是
 * @page分享页面
 * @data 2016-11-13
 */

public class ShareFragment extends HomeChildFragment implements View.OnClickListener {

    private Button btSave, btPhoto;
    private ImageView ivPhoto, ivHeadwear;
    private ImageView ivHeadwearToPhoto;
    private RelativeLayout rlEditPhoto;
    private static final int MY_PERMISSIONS_REQUEST_CALL_CAMERA = 1;
    private Uri imgUri;
    private ImageView bt_img;


    @Override
    protected View onCreateContentView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_share, container, false);
    }

    @Override
    protected void onInit() {
        super.onInit();
//        EventBus.getDefault().register(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
  ///      EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onFindViews() {
        super.onFindViews();
       bt_img    = (ImageView) mContentView.findViewById(R.id.share_img);
        /*
        btSave = (Button) mContentView.findViewById(R.id.bt_save);
        btPhoto = (Button) mContentView.findViewById(R.id.bt_take_photo);
        ivHeadwear = (ImageView) mContentView.findViewById(R.id.iv_headwear);
        ivPhoto = (ImageView) mContentView.findViewById(R.id.iv_get_photo);
        ivHeadwearToPhoto = (ImageView) mContentView.findViewById(R.id.iv_headwear_to_photo);
        rlEditPhoto = (RelativeLayout) mContentView.findViewById(R.id.rl_edit_photo);
        */
    }



    @Override
    protected void onBindListener() {
        super.onBindListener();
 /*       btPhoto.setOnClickListener(this);
        btSave.setOnClickListener(this);
        ivHeadwearToPhoto.setVisibility(View.GONE);
        ivHeadwear.setOnClickListener(this); */
    }

    @Override
    protected void onInitViewData() {
        super.onInitViewData();

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            //分享
            case R.id.bt_save:
              ///  saveView(rlEditPhoto);
                // ivHeadwearToPhoto.setVisibility(View.GONE);
                break;
            //分享给不同的平台
/*
            case R.id.bt_save:
                ///  saveView(rlEditPhoto);
                // ivHeadwearToPhoto.setVisibility(View.GONE);
                break;
            case R.id.bt_save:
                ///  saveView(rlEditPhoto);
                // ivHeadwearToPhoto.setVisibility(View.GONE);
                break;
            case R.id.bt_save:
                ///  saveView(rlEditPhoto);
                // ivHeadwearToPhoto.setVisibility(View.GONE);
                break;
                */

        }
    }


    @SuppressLint("SdCardPath")
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        ///detecter.release(getContext());// 释放引擎
    }



   public  void setImgUrl(Uri  uri,Bitmap bmp){
        imgUri = uri;
        bt_img.setImageBitmap(bmp);
    }




    private void share() {
     ShareAction shareAction = new ShareAction(getActivity());

        //分享标题
        //shareAction.withText("");

        //分享类型
//        if (mediatype == 1) {
//            shareAction.withMedia(image);
//        } else if (mediatype == 2) {
//            shareAction.withMedia(music);
//        } else if (mediatype == 3) {
//            shareAction.withMedia(video);
//        }
        //分享链接
//        shareAction.withTargetUrl(Defaultcontent.url);

        //两种方式
        //1.指定分享平台
//        shareAction.setPlatform(share_media).setCallback(umShareListener).share();
        //2.打开分享面板选择
//        shareAction.setDisplayList().setCallback(umShareListener).open();
    }


    //分享回调监听
    private UMShareListener umShareListener = new UMShareListener() {
        @Override
        public void onResult(SHARE_MEDIA platform) {
            Toast.makeText(getContext(), platform + " 分享成功啦", Toast.LENGTH_SHORT).show();
            Log.d("plat", "platform" + platform);

        }

        @Override
        public void onError(SHARE_MEDIA platform, Throwable t) {
            Toast.makeText(getContext(), platform + " 分享失败啦" + t.getMessage(), Toast.LENGTH_SHORT).show();


        }

        @Override
        public void onCancel(SHARE_MEDIA platform) {
            Toast.makeText(getContext(), platform + " 分享取消了", Toast.LENGTH_SHORT).show();
        }
    };


}
