package com.example.administrator.show.imagepicker.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.administrator.show.R;
import com.example.administrator.show.imagepicker.view.ViewPagerFixed;


import java.util.ArrayList;

import uk.co.senab.photoview.Compat;
import uk.co.senab.photoview.PhotoView;

/**
 * Created by yexiuliang on 2016/7/27.
 */

public class PhotoPreviewActivity extends AppCompatActivity {

    public static final String IMAGES = "images";
    public static final String CURRENT = "current";


    protected ViewPagerFixed mViewPager;
    private ArrayList<String> mImageItems;
    private int mCurrentPosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_preview);
        mImageItems = getIntent().getStringArrayListExtra(IMAGES);
        mCurrentPosition = getIntent().getIntExtra(CURRENT, 0);
        oninitView();
    }


    public static void show(Context context, int currentPosition, ArrayList<String> images) {
        Bundle bundle = new Bundle();
        bundle.putInt(CURRENT, currentPosition);
        bundle.putStringArrayList(IMAGES, images);
        Intent intent = new Intent(context, PhotoPreviewActivity.class);

        context.startActivity(intent,bundle);

    }


    private void oninitView() {
        mViewPager = (ViewPagerFixed) findViewById(R.id.viewpager);
        ImagePageAdapter imagePageAdapter = new ImagePageAdapter(mImageItems);
        mViewPager.setAdapter(imagePageAdapter);
        mViewPager.setCurrentItem(mCurrentPosition, false);
    }

    public class ImagePageAdapter extends PagerAdapter {
        private ArrayList<String> images = new ArrayList<>();

        public ImagePageAdapter(ArrayList<String> images) {
            this.images = images;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            PhotoView photoView = new PhotoView(PhotoPreviewActivity.this);
            Glide.with(PhotoPreviewActivity.this)
                    .load(images.get(position))
                    .error(R.drawable.default_image)
                    .placeholder(R.drawable.default_image)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(photoView);
            container.addView(photoView);
            return photoView;
        }

        @Override
        public int getCount() {
            return images.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }

    }


}
