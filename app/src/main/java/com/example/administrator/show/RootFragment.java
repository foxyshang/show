package com.example.administrator.show;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


/**
 * @page Created by yexiuliang on 2016/4/12.
 */
public abstract class RootFragment extends Fragment {

    private Bundle mSavedInstanceState;

    protected View mContentView;

    @Override
    public final View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mSavedInstanceState = savedInstanceState;
        mContentView = onCreateContentView(inflater, container, savedInstanceState);
        return mContentView;
    }

    /**
     * 子类应该实现该方法，而非Fragment原有的onCreateView
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    protected abstract View onCreateContentView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState);

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSavedInstanceState = savedInstanceState;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        onInit();
        onFindViews();
        onInitViewData();
        onBindListener();
    }

    /**
     * 由于之前的接口没设计好，onInit(), onFindViews()等回调都没有savedInstanceState参数，所以通过这个方法补上
     * @return
     */
    protected Bundle getSavedInstanceState() {
        return mSavedInstanceState;
    }

    protected void clearSavedInstanceState() {
        mSavedInstanceState = null;
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }


    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    /**
     * 初始化
     * 优先顺序：<br/>
     * <font color=red>onInit();</font><br/>
     * onFindViews();<br/>
     * onInitViewData();<br/>
     * onBindListener();<br/>
     */
    @CallSuper
    protected void onInit() {

    }

    /**
     * 查找控件 <br/>
     * 优先顺序：<br/>
     * onInit();<br/>
     * <font color=red>onFindViews();</font><br/>
     * onInitViewData();<br/>
     * onBindListener();<br/>
     */
    @CallSuper
    protected void onFindViews() {

    }


    /**
     * 初始化控件内容
     * 优先顺序：<br/>
     * onInit();<br/>
     * onFindViews();<br/>
     * <font color=red>onInitViewData();</font><br/>
     * onBindListener();<br/>
     */
    @CallSuper
    protected void onInitViewData() {

    }

    /**
     * 注册控件事件
     * 优先顺序：<br/>
     * onInit();<br/>
     * onFindViews();<br/>
     * onInitViewData();<br/>
     * <font color=red>onBindListener();</font><br/>
     */
    @CallSuper
    protected void onBindListener() {

    }


}
