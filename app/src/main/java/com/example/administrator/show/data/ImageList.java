package com.example.administrator.show.data;

import com.example.administrator.show.imagepicker.bean.ImageItem;

import java.util.List;

/**
 * @author yexiuliang
 * @page
 * @data 2016-11-13
 */

public class ImageList {

    public List<ImageItem> imageList;
    public boolean has;

    public ImageList(List<ImageItem> imageList, boolean has) {
        this.imageList = imageList;
        this.has = has;
    }
}
