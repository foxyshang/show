package com.example.administrator.show.home;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.example.administrator.show.HomeChildFragment;
import com.example.administrator.show.R;
import com.example.administrator.show.photo.PhotoFragment;


/**
 * @author yexiuliang
 * @page
 * @data 2016-11-13
 */

public class HomeFragment extends HomeChildFragment {
    private View view;
    private WebView objwv;
    private homeFraInterface homeF;
    // private WebView objwv;


    public interface homeFraInterface {
///    public void setweburl(String title);

        public void changeNav();
    }
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            homeF = (homeFraInterface)   activity;
        } catch (Exception e) {
            throw new ClassCastException(activity.toString() + "must implement  ");
        }
    }

    @Override
    protected View onCreateContentView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
      //  return inflater.inflate(R.layout.fragment_home, container, false);

        view = inflater.inflate(R.layout.fragment_home, container, false);

        objwv = (WebView) view.findViewById(R.id.wv);
        objwv.loadUrl("http://guocui.vqo.cn/Admin/index.php/Wap/index");
        WebSettings webSettings = objwv.getSettings();
        webSettings .setJavaScriptEnabled(true);
        objwv.setWebViewClient(new WebViewClient(){

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String request) {

                 Log.v("home","页面跳转"+request.toString());

                homeF.changeNav();
                objwv.loadUrl(request);
                return super.shouldOverrideUrlLoading(view, request);
            }
        });

       /// objwv.
        return view;
    }

      public  void   goback(){

       //   if(keyCode == KeyEvent.KEYCODE_BACK ){

              if(objwv!=null &&objwv.canGoBack()){

                  objwv.goBack();

              }else{
                  //  finish();

              }
            ///  return  true;
         /// }


        }


    public void  seturl(){



   //  objwv.loadUrl("http://guocui.vqo.cn/Admin/index.php/Wap/index");


//        WebSettings settings = objwv.getSettings();
      //settings.setUseWideViewPort(true);
      ///settings.setLoadWithOverviewMode(true);
      ///    settings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NARROW_COLUMNS);


  }



}
