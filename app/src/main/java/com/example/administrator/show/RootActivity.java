package com.example.administrator.show;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.v4.app.FragmentActivity;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

/**
 * @author yexiuliang
 * @page
 * @data 2016-11-13
 */

public class RootActivity extends FragmentActivity {

    private App mApp;


    private boolean mHasDestroyed;

    private Bundle mSavedInstanceState;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mSavedInstanceState = savedInstanceState;


     mApp = (App) this.getApplication();
     mApp.addActivity(this);
    }


    protected Bundle getSavedInstanceState() {
        return mSavedInstanceState;
    }

    protected void clearSavedInstanceState() {
        mSavedInstanceState = null;
    }

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        onSetContentView();
    }

    @Override
    public void setContentView(View view) {
        super.setContentView(view);
        onSetContentView();
    }

    @Override
    public void setContentView(View view, ViewGroup.LayoutParams params) {
        super.setContentView(view, params);
        onSetContentView();
    }

    private void onSetContentView() {
        //调用顺序
        onInit();
        onFindViews();
        onInitViewData();
        onBindListener();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @CallSuper
    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    @Override
    public void finish() {
        super.finish();
    }

    /**
     * activity 销毁时，清除与之对应的还没有完成的图片下载任务。
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        mHasDestroyed = true;
    }

    public boolean hasDestroyed() {
        return mHasDestroyed;
    }

    /**
     * 初始化
     * 优先顺序：<br/>
     * <font color=red>onInit();</font><br/>
     * onFindViews();<br/>
     * onInitViewData();<br/>
     * onBindListener();<br/>
     */
    @CallSuper
    protected void onInit() {

    }

    /**
     * 查找控件 <br/>
     * 优先顺序：<br/>
     * onInit();<br/>
     * <font color=red>onFindViews();</font><br/>
     * onInitViewData();<br/>
     * onBindListener();<br/>
     */
    @CallSuper
    protected void onFindViews() {

    }


    /**
     * 初始化控件内容
     * 优先顺序：<br/>
     * onInit();<br/>
     * onFindViews();<br/>
     * <font color=red>onInitViewData();</font><br/>
     * onBindListener();<br/>
     */
    @CallSuper
    protected void onInitViewData() {

    }

    /**
     * 注册控件事件
     * 优先顺序：<br/>
     * onInit();<br/>
     * onFindViews();<br/>
     * onInitViewData();<br/>
     * <font color=red>onBindListener();</font><br/>
     */
    @CallSuper
    protected void onBindListener() {

    }


}
