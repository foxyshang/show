package com.example.administrator.show;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.Application;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.umeng.socialize.PlatformConfig;
import com.umeng.socialize.UMShareAPI;

import java.util.ArrayList;
import java.util.List;

/**
 * @author yexiuliang
 * @page
 * @data 2016-11-12
 */

public class App extends Application {

    //各个平台的配置，建议放在全局Application或者程序入口
    {
        //微信 wx12342956d1cab4f9,a5ae111de7d9ea137e88a5e02c07c94d
        PlatformConfig.setWeixin("wxdc1e388c3822c80b", "3baf1193c85774b3fd9d18447d76cab0");

        //新浪微博
      //  PlatformConfig.setSinaWeibo("3921700954", "04b48b094faeb16683c32669824ebdad");
        //易信
      // PlatformConfig.setQQZone("100424468", "c7394704798a158208a74ab60104f0ba");
      //  PlatformConfig.setTwitter("3aIN7fuF685MuZ7jtXkQxalyi", "MK6FEYG63eWcpDFgRYw4w9puJhzDl0tyuqWjZ3M7XJuuG7mMbO");
    }


    private ArrayList<Activity> mActivities;
//    public static PfConfig PF;


    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
//        dex 目前用不到
//        MultiDex.install(this);
    }


    @Override
    public void onCreate() {
        super.onCreate();
        Context cxt = getApplicationContext();
        UMShareAPI.get(cxt);
        //noinspection PointlessBooleanExpression,ConstantConditions
//        if (!Config.DEBUG) {
////            MobclickAgent.setCatchUncaughtExceptions(false);
//            CrashHandlerUtil crashHandler = CrashHandlerUtil.getInstance();
//            crashHandler.init(cxt);
//            crashHandler.setCrashFile(Config.APP_CRASH_LOG_PATH);
//        } else {
//            VersionUtils.enableStrictMode();
//        }
//        PF = new PfConfig(cxt);
        if (!TextUtils.equals(getCurProcessName(getApplicationContext()), getApplicationInfo().processName)) {
            //非主进程，不执行后面的初始化，如push进程
            return;
        }
        mActivities = new ArrayList<Activity>();

    }


    /**
     * 添加一个页面到集合中
     *
     * @param a
     */
    public void addActivity(Activity a) {
        mActivities.add(a);
    }

    /**
     * 移除栈
     */
    public void removeActivity(Activity activity) {
        if (activity == null) {
            return;
        }

        int index = mActivities.indexOf(activity);
        if (index != -1) {
            mActivities.remove(activity);
            if (!activity.isFinishing()) {
                activity.finish();
            }
        }
    }


    public int getActivityCount() {
        return mActivities.size();
    }


    public Activity getLastActivity() {
        int size = mActivities.size();
        if (size > 0) {
            return mActivities.get(size - 1);
        } else {
            return null;
        }
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    /**
     * 获得当前进程的进程名
     *
     * @param context context
     * @return 当前进程的进程名
     */
    public String getCurProcessName(Context context) {
        int pid = android.os.Process.myPid();

        ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);

        List<ActivityManager.RunningAppProcessInfo> runningProcess = activityManager.getRunningAppProcesses();

        for (ActivityManager.RunningAppProcessInfo info : runningProcess) {
            if (info.pid == pid) {
                return info.processName;
            }
        }

        return null;
    }

}
