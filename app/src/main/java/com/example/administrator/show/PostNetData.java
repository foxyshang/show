package com.example.administrator.show;

        import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.io.IOException;
        import java.net.URI;
        import java.net.URISyntaxException;

        import static com.example.administrator.show.R.string.guocui_url_1;


public class PostNetData {
    String app_code;
    String service_code;
    String screen_code;

    //get形式
    public  PostNetData(){

    }

    public  void setpostheader(String app_code,String service_code,String screen_code){
        this.app_code = app_code;
        this.service_code = service_code;
        this.screen_code = screen_code;
    }

    public void setpostbody(JSONObject obj){

    }

    //
    public static String postResultForHttpGet() throws ClientProtocolException, IOException, URISyntaxException {

        String result="";
        HttpClient httpclient = new DefaultHttpClient();

        HttpPost httppost = new HttpPost(new URI( "http://guocui.vqo.cn/Admin/index.php/Api/index" ) );
        Log.v("test",String.valueOf(R.string.guocui_url_1));
        //添加http头信息
        httppost.addHeader("Content-Type", "application/json;charset=UTF-8");

       /* if(obj!=null){

            httppost.setEntity(new StringEntity(obj.toString(), "utf-8"));
        }*/
        HttpResponse response;

        response = httpclient.execute(httppost);

        //检验状态码，如果成功接收数据
        int code = response.getStatusLine().getStatusCode();
        Log.v("ccc","code"+code);
        if (code == 200) {
            result = EntityUtils.toString(response.getEntity());//返回json格式： {"id": "27JpL~j4vsL0LX00E00005","version": "abc"}
            Log.v("HHH",result);

        }
        return result;

    }

}
